# Gatling Template gRPC

## Описание 
Данный код построен на основе [gatling-grpc plugin](https://github.com/phiSgr/gatling-grpc). Больше информации о данном плагине можно найти в статье [A Demo of Gatling-gRPC](https://medium.com/@georgeleung_7777/a-demo-of-gatling-grpc-bc92158ca808).
В качестве сервиса, который тестируется используется: photoslibrary.googleapis.com и метод `METHOD_CHECK` для него. 
  
## Life hack
Перед первым запуском вам надо сгенерировать классы java на основе protobuf файла. 
Для этого вам надо запустить тест, но закомментировать все `import io.grpc.health.v1._`. После запуска в папке `\target\scala-2.12\src_managed\test\io\grpc\health\v1` будут созданы java классы если всё прошло успешно, расскоментируйте `import`.  

## Запуск сценариев нагрузки
Запуск тестов происходит по 3 заранее готовым сценариям:
* `"gatling:testOnly *MaxPerformnaceTest"` - запуск теста максимальной производительности;
* `"gatling:testOnly *Stability"` - запуск теста стабильности;
* `"gatling:testOnly *DebugTest"` - запуск отладочного теста, все запросы его проксируются на порт 8888. 

Параметры запусков тестов определены в файле `simulation.conf`. 